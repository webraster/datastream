require('dotenv').config({})
const redis = require('redis')
const ws = require('ws')

const client = redis.createClient({ url: process.env.REDIS_URL })

const server = new ws.Server({
  port: 80,
  clientTracking: true,
  noServer: true,
})

server.on('connection', async (socket, req) => {
  console.log('socket connected')
  socket.send(JSON.stringify({
    queueLength: await queueLength(),
  }))
})

server.on('close', () => console.log('socket disconnected'))

// Redis subscription
const sub = redis.createClient({ url: process.env.REDIS_URL })

sub.subscribe('url_image_completion', (err, arg) => {
  if (err) return console.log('Error subscribing', err)
  console.log('subscribed to channel', arg)
})

sub.on('message', async (channel, message) => {
  console.log('message', message)
  const data = {
    queueLength: await queueLength(),
  }
  try {
    const parsed = JSON.parse(message)
    Object.assign(data, parsed)
  } catch (err) {
    console.log('Error parsing redis message data', message)
  }
  broadcast(JSON.stringify(data))
})

function broadcast(message) {
  server.clients.forEach((_client) => {
    if (_client.readyState === ws.OPEN) {
      _client.send(message)
    }
  })
}

async function queueLength() {
  return await new Promise((rs, rj) => {
    client.llen('url_image_requests', (err, count) => {
      if (err) return rj(err)
      rs(count)
    })
  })
}
