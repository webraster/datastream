FROM alpine:edge
MAINTAINER voidreturn

RUN apk add --no-cache nodejs-npm

COPY . /src
WORKDIR /src

RUN npm ci

CMD ["node", "/src"]
